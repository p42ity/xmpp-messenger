# xmpp-messenger

### Read this in other languages: <a href="https://codeberg.org/p42ity/xmpp-messenger/src/branch/main/README-en.md">English</a>

# Wissenswertes zur XMPP Messenger Tabelle [FAQ] 

## Schwerpunkt

Diese Tabelle konzentriert sich auf Messenger, die den offenen Standard <a href="https://xmpp.org/">XMPP</a> verwenden.

## Warum ist Messenger XY nicht dabei?

Eine tolle Übersicht von XMPP-Messengern bietet die Seite <a href="https://xmpp.org/software/">xmpp.org</a>. Auf dieser Seite wird auch die Compliance (und viele weitere Details) der einzelnen Messenger angegeben. Die Auswahl in dieser Tabelle beschränkt sich auf Messenger,
die eine gewisse Basisfunktionalität zur Verfügung stellen. Es handelt sich somit um eine subjektive Auswahl.

## Bewertung / Beurteilung

Die Inhalte der Tabelle sind von mir nach eigenen Maßstäben gewählt. Wenn ihr weitere Inhalte für sinnvoll haltet oder Fehler findet, dann kontaktiert mich gerne.

## Subjektive Einschätzung 

Die subjektive Einschätzung basiert auf meiner persönlichen Erfahrung mit dem jeweiligen Messenger. Bitte betrachtet diese Einschätzung nicht als gesetzt, sondern verschafft euch gerne ein eigenes Bild.

# Copyright und Lizenz

Die XMPP-Messenger Tabelle wird erstellt durch <a href="https://eversten.net/">eversten.net</a>. Sie basiert auf dem Quellcode der <a href="https://messenger-matrix.de">Messenger-Matrix</a>, um eine gut lesbare Tabellenansicht zu gewährleisten. Der Inhalt der Tabelle ist jedoch komplett unterschiedlich. Somit steht auch dieser Quellcode unter der <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de">CC BY-SA 4.0-Lizenz</a>.
