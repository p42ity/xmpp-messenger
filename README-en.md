# xmpp-messenger

### Read this in other languages: <a href="https://codeberg.org/p42ity/xmpp-messenger/src/branch/main/README.md">German</a>

# Things to know about the XMPP messenger table [FAQ]

## Focus

This table focuses on messenger which use the open standard <a href="https://xmpp.org/">XMPP</a>.

## Why is messenger XY not included?

You can find a fantastic overview of XMPP messengers at <a href="https://xmpp.org/software/">xmpp.org</a>. You can also find the compliance of the different messengers there. The selection in my XMPP messenger table focuses on messenger which provide some kind of
basis functionality which I think is necessary to convince people to try an app.


## Evaluation / Assessment

I selected the criteria in this table. If there are missing criterias or wrong information in this table, please do not hesitate to contact me.

## Subjective assessment

The assessment in this table is subjective and is based on my experience when using those apps. Please do not simply rely on this table. Do not hesitate and try the apps by yourself.

# Copyright and license

The XMPP messenger table is created by <a href="https://eversten.net">eversten.net</a> and based on the source code of the <a href="https://messenger-matrix.de/">Messenger Matrix</a> and is therefore licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de">CC BY-SA 4.0-Lizenz</a>.
